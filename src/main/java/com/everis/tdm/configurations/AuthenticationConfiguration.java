package com.everis.tdm.configurations;

import com.everis.tdm.config.PropertyManager;

public class AuthenticationConfiguration {
    public static String getTokenUser() {
        return (String) PropertyManager.getInstance().get("token.user");
    }
    public static String getTokenPass() {
        return (String) PropertyManager.getInstance().get("token.pass");
    }
}
