package com.everis.tdm.configurations;

import com.everis.tdm.config.PropertyManager;

public class ServicesConfiguration {
    public static String getBaseUrl() {
        return (String) PropertyManager.getInstance().get("base.url");
    }

    public static String getAssiSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("assi.subscription.key");
    }

    public static String getSecuritySubscriptionKey() {
        return (String) PropertyManager.getInstance().get("security.subscription.key");
    }

    public static String getAssiCommonSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("assi.common.subscription.key");
    }

    public static String getAssiCreditcardSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("assi.creditcard.subscription.key");
    }

    public static String getCustomerSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("customer.subscription.key");
    }

    public static String getBenefitsSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("benefits.subscription.key");
    }

    public static String getProductsSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("products.subscription-key");
    }

    public static String getCreditCardSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("creditcard.subscription.key");
    }

    public static String getApiConnectUrl() {
        return (String) PropertyManager.getInstance().get("api.connect.url");
    }

    public static String getApiConnectCredentials() {
        return (String) PropertyManager.getInstance().get("api.connect.basic");
    }

    public static String getApiConnectToken() {
        return (String) PropertyManager.getInstance().get("api.connect.token.assi");
    }

    public static String getCustomerNaturalSubscriptionKey() {
        return (String) PropertyManager.getInstance().get("customer.natural.subscription.key");
    }
}
