package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class RegistroGestion {

    private String NroTramite;
    private String Usuario;
    private String FuenteData;
    private String TipoRegistro;
    private String TipoDocumento;
    private String NumDocumento;
    private String IdGestion;
    private String Llamada;
    private String Comentario;
}
