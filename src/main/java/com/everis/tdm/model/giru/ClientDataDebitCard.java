package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientDataDebitCard {
    private String uniqueCode;
    private String typeClient;
    private String documentClient;
    private String typeDocument;
    private String firstFirstName;
    private String secondFirstName;
    private String secondLastName;
    private String situation;
    private String cardNumber;
    private String status_code;

}
