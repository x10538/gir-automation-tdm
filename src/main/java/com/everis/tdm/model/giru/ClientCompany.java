package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientCompany {
    private String uniqueCode;
    private String name;
    private String admissionDate;
    private String address;
    private String phoneNumber;
    private String extension;
    private String positionDescripcion;
    private String occupation;
    private String independent;
    private String ubigeo;
    private String employeeIndicator;

}
