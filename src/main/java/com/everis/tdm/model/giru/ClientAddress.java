package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
@ToString
@Getter
@Setter
public class ClientAddress {
    //private String uniqueCode;
    private String useCode;
    private String sequenceId;
    private String department;
    private String province;
    private String district;
    private String streetType;
    private String streetName;
    private String streetNumber;
    private String block;
    private String lot;
    private String apartment;
    private String neighborhood;
    private String landmark;
    private String postalCode;
    private String country;
    private String ubigeo;
    private String type;
    private String useType;
    private String date;
    private String status;
    private String flagStandard;
}
