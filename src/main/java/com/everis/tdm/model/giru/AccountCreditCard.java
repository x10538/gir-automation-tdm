package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class AccountCreditCard {
    private String accountNumber;
    private String currency;
    private String creditLine;
    private List<CreditCard> creditCard;

    public AccountCreditCard(String accountNumber, String currency, String creditLine, List<CreditCard> creditCard) {
        this.accountNumber = accountNumber;
        this.currency = currency;
        this.creditLine = creditLine;
        this.creditCard = creditCard;

    }

    public AccountCreditCard(){}
}
