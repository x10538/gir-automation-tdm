package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerTipologiaIdMapperRetail implements RowMapper<ObtenerTipologiaIdRetail> {

    @SneakyThrows
    public ObtenerTipologiaIdRetail mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerTipologiaIdRetail data = new ObtenerTipologiaIdRetail();

        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setTipoTramite(rs.getString("TipoTramite"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setIdTipologiaElement(rs.getString("IdTipologiaElement"));
        data.setEstadoTipologia(rs.getString("setEstadoTipologia"));
        return data;
    }
}
