package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class ObtenerTipologiaIdRetail {
    private String Ambiente;
    private String Producto;
    private String TipoTramite;
    private String NombreTipologia;
    private String IdTipologiaElement;
    private String EstadoTipologia;
}
