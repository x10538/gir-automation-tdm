package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientData2 {
    private String uniqueCode;
    private String typeClient;
    private String documentClient;
    private String typeDocument;
    private String firstFirstName;
    private String secondFirstName;
    private String firstLastName;
    private String secondLastName;
    private String cellNumber;
    private String profileLevel;
    private String profileLevelStatus;
    private String statusProfileLevel;
    private String profileLevelChange;

//    public ClientData2() {
////        this.accountNumber = accountNumber;
////        this.creditLine = creditLine;
//        //this.accounts = new ArrayList<>();
//    }
//
//    public void addAccountData(Account a) {
//        this.accounts.add(a);
//    }
//
//    public int countAccountData() {
//        return this.accounts.size();
//    }
}
