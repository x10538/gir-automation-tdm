package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EscenarioValidacionMapper implements RowMapper<EscenarioValidacion> {
    @SneakyThrows
    public EscenarioValidacion mapRow(ResultSet rs, int arg1) throws SQLException {
        EscenarioValidacion data = new EscenarioValidacion();
        data.setTipoProducto(rs.getString("tipoProducto"));
        data.setTipoTramite(rs.getString("tipoTramite"));
        data.setTipologia(rs.getString("tipologia"));
        data.setAreaInicial(rs.getString("areaInicial"));

        data.setAreaValidadoraOpcional(rs.getString("areaValidadoraOpcional"));
        data.setAreaDevuelta(rs.getString("areaDevuelta"));
        data.setAreaResolutora(rs.getString("areaResolutora"));
        data.setFase(rs.getString("fase"));

        return data;
    }

}