package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class RegistroReclamoPOC {

    private String NroTramite;
    private String Tipologia;
    private String FechaRegistro;
    private String UsuarioRegistrado;
    private String Perfil;
    private String TipoDocumento;
    private String NumeroDocumento;
    private String Tramite;
    private String Atencion;


}
