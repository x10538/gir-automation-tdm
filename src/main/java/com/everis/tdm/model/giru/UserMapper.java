package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class UserMapper implements RowMapper<User> {
    @SneakyThrows
    public User mapRow(ResultSet rs, int arg1) throws SQLException {
        User data = new User();
        data.setUsuario(rs.getString("usuario"));
        data.setContrasena(rs.getString("contrasena"));
        data.setRol(rs.getString("rol"));
        data.setArea(rs.getString("area"));
        data.setAmbiente(rs.getString("ambiente"));
        data.setPerfil(rs.getString("perfil"));
        return data;
    }
}