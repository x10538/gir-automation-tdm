package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class ClientCreditCard {
    private String uniqueCode;
    private String documentClient;
    private String typeDocument;
    private String cardNumber;
    private String accountNumber;
    private String currency;
    private String organisationId;
    private String branchCode;
    private String contractNumber;
    private String firstName;
    private String middleName;
    private String lastName;
    private String lastName2;
}
