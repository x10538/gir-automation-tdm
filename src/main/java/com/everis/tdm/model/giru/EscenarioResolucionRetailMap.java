package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EscenarioResolucionRetailMap implements RowMapper<EscenarioResolucionRetail> {
    public EscenarioResolucionRetail mapRow(ResultSet rs, int arg1) throws SQLException {
        EscenarioResolucionRetail escenarioResolucionRetail = new EscenarioResolucionRetail();
        escenarioResolucionRetail.setTipoProducto(rs.getString("tipoProducto"));
        escenarioResolucionRetail.setTipoTramite(rs.getString("tipoTramite"));
        escenarioResolucionRetail.setNombreTipologia(rs.getString("nombreTipologia"));
        escenarioResolucionRetail.setAreaInicial(rs.getString("areaInicial"));
        escenarioResolucionRetail.setAreaValidadoraOpcional(rs.getString("areaValidadoraOpcional"));
        escenarioResolucionRetail.setAreaDevuelta(rs.getString("areaDevuelta"));
        escenarioResolucionRetail.setAreaResolutora(rs.getString("areaResolutora"));
        escenarioResolucionRetail.setExisteMasdeUnaAreaValidadora(rs.getString("existeMasdeUnaAreaValidadora"));
        escenarioResolucionRetail.setCodAgrupamiento(rs.getString("codAgrupamiento"));
        escenarioResolucionRetail.setIdTipologiaElement(rs.getString("idTipologiaElement"));
        escenarioResolucionRetail.setProcede(rs.getString("procede"));
        escenarioResolucionRetail.setNoProcede(rs.getString("noProcede"));
        escenarioResolucionRetail.setDeriva(rs.getString("deriva"));
        return escenarioResolucionRetail;
    }
}
