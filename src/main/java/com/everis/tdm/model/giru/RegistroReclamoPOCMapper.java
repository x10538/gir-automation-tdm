package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegistroReclamoPOCMapper implements RowMapper<RegistroReclamoPOC> {
    @SneakyThrows
    public RegistroReclamoPOC mapRow(ResultSet rs, int arg1) throws SQLException {
        RegistroReclamoPOC data = new RegistroReclamoPOC();

        data.setNroTramite(rs.getString("NroTramite"));
        data.setTipologia(rs.getString("Tipologia"));

        data.setFechaRegistro(rs.getString("@FechaRegistro"));


        data.setUsuarioRegistrado(rs.getString("@UsuarioRegistrado"));
        data.setPerfil(rs.getString("Perfil"));

        data.setTipoDocumento(rs.getString("TipoDocumento"));
        data.setNumeroDocumento(rs.getString("NumeroDocumento"));

        data.setTramite(rs.getString("Tramite"));
        data.setAtencion(rs.getString("Atencion"));



        return data;
    }
}



