package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class DebitCard {
    private String uniqueCode;
    private String cardNumber;
    private String status_code;
    private String status_description;
    private String block_id;
    private String customer_id;
    private String customer_type;
    private String branchId;
    private String openingDate;
    private String dueDate;
    private String cardGroup;
    private String cardName;
    private String requestId;
    private String status;
    private String embossingtype;
    private String cardSubType;
    private String cardTechType;
    private String cardType;
    private String lastReferenceNumber;
    private String previousReferenceNumber;
    private String lock_reasonId;

    public DebitCard(String uniqueCode,
                     String cardNumber,
                     String status_code,
                     String status_description,
                     String block_id,
                     String customer_id,
                     String customer_type,
                     String branchId,
                     String openingDate,
                     String dueDate,
                     String cardGroup,
                     String cardName,
                     String requestId,
                     String status,
                     String embossingtype,
                     String cardSubType,
                     String cardTechType,
                     String cardType,
                     String lastReferenceNumber,
                     String previousReferenceNumber,
                     String lock_reasonId) {

        this.uniqueCode=uniqueCode;
                this.cardNumber=cardNumber;
                this.status_code=status_code;
                this.status_description=status_description;
                this.block_id=block_id;
                this.customer_id=customer_id;
                this.customer_type=customer_type;
                this.branchId=branchId;
                this.openingDate=openingDate;
                this.dueDate=dueDate;
                this.cardGroup=cardGroup;
                this.cardName=cardName;
                this.requestId=requestId;
                this.status=status;
                this.embossingtype=embossingtype;
                this.cardSubType=cardSubType;
                this.cardTechType=cardTechType;
                this.cardType=cardType;
                this.lastReferenceNumber=lastReferenceNumber;
                this.previousReferenceNumber=previousReferenceNumber;
                this.lock_reasonId=lock_reasonId;
    }

    public DebitCard() {
    }
}
