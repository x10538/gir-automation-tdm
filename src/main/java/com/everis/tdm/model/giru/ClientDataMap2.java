package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDataMap2 implements RowMapper<ClientData2> {
    public ClientData2 mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientData2 clientData2 = new ClientData2();
        clientData2.setUniqueCode(rs.getString("uniqueCode"));
        clientData2.setTypeClient(rs.getString("typeClient"));
        clientData2.setDocumentClient(rs.getString("documentClient"));
        clientData2.setTypeDocument(rs.getString("typeDocument"));
        clientData2.setFirstFirstName(rs.getString("firstFirstName"));
        clientData2.setSecondFirstName(rs.getString("secondFirstName"));
        clientData2.setFirstLastName(rs.getString("firstLastName"));
        clientData2.setSecondLastName(rs.getString("secondLastName"));
        clientData2.setCellNumber(rs.getString("phoneNumber"));
        return clientData2;
    }
}
