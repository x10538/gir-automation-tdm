package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class CreditCard {
    private String accountNumber;
    private String cardNumber;
    private String beneficiary;
    private String mark;
    private String situation;
    private String status;
    private String typeCode;
    private String type;
    private String activationDate;

    public CreditCard(String accountNumber, String cardNumber, String beneficiary, String mark, String situation, String status, String typeCode, String type, String activationDate) {
        this.accountNumber = accountNumber;
        this.cardNumber = cardNumber;
        this.beneficiary = beneficiary;
        this.mark = mark;
        this.situation = situation;
        this.status = status;
        this.typeCode = typeCode;
        this.type = type;
        this.activationDate = activationDate;
    }

    public CreditCard() {
    }

}
