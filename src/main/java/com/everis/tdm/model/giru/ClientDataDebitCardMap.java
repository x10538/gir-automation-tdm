package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDataDebitCardMap implements RowMapper<ClientDataDebitCard> {
    public ClientDataDebitCard mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientDataDebitCard clientDataDebitCard = new ClientDataDebitCard();
        clientDataDebitCard.setUniqueCode(rs.getString("uniqueCode"));
        clientDataDebitCard.setTypeClient(rs.getString("typeClient"));
        clientDataDebitCard.setDocumentClient(rs.getString("documentClient"));
        clientDataDebitCard.setTypeDocument(rs.getString("typeDocument"));
        clientDataDebitCard.setFirstFirstName(rs.getString("firstFirstName"));
        clientDataDebitCard.setSecondFirstName(rs.getString("secondFirstName"));
        clientDataDebitCard.setSecondLastName(rs.getString("secondLastName"));
        clientDataDebitCard.setSituation(rs.getString("situation"));
        clientDataDebitCard.setCardNumber(rs.getString("cardNumber"));
        clientDataDebitCard.setStatus_code(rs.getString("status_code"));
        return clientDataDebitCard;
    }
}