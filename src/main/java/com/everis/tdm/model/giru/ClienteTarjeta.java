package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@ToString
@Setter
public class ClienteTarjeta {

    private String tipodocumento;
    private String numerodocumento;
    private String numerotarjeta;
    private String tipotarjeta;
    private String marcatarjeta;
    private String estadoproducto;

    private String tipoproducto;
    private String monedacuenta;
    private String numerocuenta;
    private String tipodecuenta;
}

