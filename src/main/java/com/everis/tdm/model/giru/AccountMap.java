package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AccountMap implements RowMapper<Account> {
    public Account mapRow(ResultSet rs, int arg1) throws SQLException {
        Account account = new Account();
        account.setType(rs.getString("type"));
        account.setProductCode(rs.getString("productCode"));
        account.setSubType(rs.getString("subType"));
        account.setSubProductCode(rs.getString("subProductCode"));
        account.setCurrency(rs.getString("currency"));
        account.setBalance(rs.getString("balance"));
        account.setAvailableBalance(rs.getString("availableBalance"));
        account.setStatus(rs.getString("status"));
        account.setOffice(rs.getString("office"));
        account.setAccountNumber(rs.getString("accountNumber"));
        account.setJoint_type(rs.getString("joint_type"));
        account.setCci(rs.getString("cci"));
        account.setId(rs.getString("id"));
        return account;
    }
}
