package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerMotivoIdMapperRetail implements RowMapper<ObtenerMotivoIdRetail> {
    @SneakyThrows
    public ObtenerMotivoIdRetail mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerMotivoIdRetail data = new ObtenerMotivoIdRetail();

        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setNombreMotivo(rs.getString("NombreMotivo"));
        data.setIdMotivo(rs.getString("IdMotivo"));
        data.setEstadoMotivo(rs.getString("EstadoMotivo"));
        data.setMotivoTransaccional(rs.getString("motivoTransaccional"));


        return data;
    }
}
