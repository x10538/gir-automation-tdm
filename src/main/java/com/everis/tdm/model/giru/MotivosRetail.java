package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class MotivosRetail {
    private String ambiente;
    private String producto;
    private String nombreTipologia;
    private String idTipologiaElement;
    private String nombreMotivo;
    private String idMotivo;
    private String estadoMotivo;
    private String motivoTransaccional;
}
