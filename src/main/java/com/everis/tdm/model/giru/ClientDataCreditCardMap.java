package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ClientDataCreditCardMap implements RowMapper<ClientDataCreditCard> {
    public ClientDataCreditCard mapRow(ResultSet rs, int arg1) throws SQLException {
        ClientDataCreditCard clientDataCreditCard = new ClientDataCreditCard();
        clientDataCreditCard.setUniqueCode(rs.getString("uniqueCode"));
        clientDataCreditCard.setTypeClient(rs.getString("typeClient"));
        clientDataCreditCard.setDocumentClient(rs.getString("documentClient"));
        clientDataCreditCard.setTypeDocument(rs.getString("typeDocument"));
        clientDataCreditCard.setFirstFirstName(rs.getString("firstFirstName"));
        clientDataCreditCard.setSecondFirstName(rs.getString("secondFirstName"));
        clientDataCreditCard.setSecondLastName(rs.getString("secondLastName"));
        clientDataCreditCard.setSituation(rs.getString("situation"));
        clientDataCreditCard.setCardNumber(rs.getString("cardNumber"));
        clientDataCreditCard.setBeneficiary(rs.getString("beneficiary"));
        clientDataCreditCard.setMark(rs.getString("mark"));
        clientDataCreditCard.setStatus(rs.getString("status"));
        clientDataCreditCard.setTypeCode(rs.getString("typeCode"));
        clientDataCreditCard.setType(rs.getString("type"));
        clientDataCreditCard.setAccountNumber(rs.getString("accountNumber"));
        return clientDataCreditCard;
    }
}