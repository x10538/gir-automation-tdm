package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class ClienteMapper implements RowMapper<Cliente> {
    @SneakyThrows
    public Cliente mapRow(ResultSet rs, int arg1) throws SQLException {
        Cliente data = new Cliente();
        data.setNrodocumento(rs.getString("nrodocumento"));
        data.setTipodocumento(rs.getString("tipodocumento"));
        data.setOperador(rs.getString("operador"));
        data.setTelefono(rs.getString("telefono"));
        data.setCantidadTarjetas(rs.getString("cantidadTarjetas"));
        return data;
    }
}