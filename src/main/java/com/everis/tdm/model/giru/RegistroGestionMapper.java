package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RegistroGestionMapper implements RowMapper<RegistroGestion> {
    @SneakyThrows
    public RegistroGestion mapRow(ResultSet rs, int arg1) throws SQLException {
        RegistroGestion data = new RegistroGestion();
        data.setNroTramite(rs.getString("NroTramite"));
        data.setUsuario(rs.getString("Usuario"));
        data.setFuenteData(rs.getString("FuenteData"));
        data.setTipoRegistro(rs.getString("TipoRegistro"));
        data.setTipoDocumento(rs.getString("TipoDocumento"));
        data.setNumDocumento(rs.getString("NumDocumento"));
        data.setIdGestion(rs.getString("IdGestion"));
        data.setLlamada(rs.getString("Llamada"));
        data.setComentario(rs.getString("Comentario"));
        return data;
    }
}



