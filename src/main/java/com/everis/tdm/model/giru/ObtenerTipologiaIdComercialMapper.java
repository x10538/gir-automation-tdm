package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ObtenerTipologiaIdComercialMapper implements RowMapper<ObtenerTipologiaIdComercial> {
    @SneakyThrows
    public ObtenerTipologiaIdComercial mapRow(ResultSet rs, int arg1) throws SQLException {
        ObtenerTipologiaIdComercial data = new ObtenerTipologiaIdComercial();
        
        data.setAmbiente(rs.getString("Ambiente"));
        data.setProducto(rs.getString("Producto"));
        data.setTipoTramite(rs.getString("TipoTramite"));
        data.setNombreTipologia(rs.getString("NombreTipologia"));
        data.setIdTipologiaElement(rs.getString("IdTipologiaElement"));

        return data;
    }

}