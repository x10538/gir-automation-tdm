package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.util.List;

@ToString
@Getter
@Setter
public class EscenarioResolucionRetail {
    private String tipoProducto;
    private String tipoTramite;
    private String nombreTipologia;
    private String areaInicial;
    private String areaValidadoraOpcional;
    private String areaDevuelta;
    private String areaResolutora;
    private String existeMasdeUnaAreaValidadora;
    private String codAgrupamiento;
    private String idTipologiaElement;
    private String procede;
    private String noProcede;
    private String deriva;
}
