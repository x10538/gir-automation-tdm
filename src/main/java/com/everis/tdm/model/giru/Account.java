package com.everis.tdm.model.giru;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@ToString
@Getter
@Setter
public class Account {
    private String type;
    private String productCode;
    private String subType;
    private String subProductCode;
    private String currency;
    private String finantialInstitution;
    private String balance;
    private String availableBalance;
    private String status;
    private String office;
    private String accountNumber;
    //private JsonbMapper joint;
    private String cci;
    private String id;
    private String joint_type;

    public Account(String type, String productCode, String subType, String subProductCode,
                   String currency, String balance, String availableBalance, String status,
                   String office, String accountNumber, String joint_type, String cci,
                   String id) {
        this.type = type;
        this.productCode = productCode;
        this.subType = subType;
        this.subProductCode = subProductCode;
        this.currency = currency;
        this.balance = balance;
        this.availableBalance = availableBalance;
        this.status = status;
        this.office = office;
        this.accountNumber = accountNumber;
        this.joint_type = joint_type;
        this.cci = cci;
        this.id = id;
    }

    public Account(){}
}
