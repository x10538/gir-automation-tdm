package com.everis.tdm.model.giru;

import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class MotivosRetailMap implements RowMapper<MotivosRetail> {
    public MotivosRetail mapRow(ResultSet rs, int arg1) throws SQLException {
        MotivosRetail motivosRetail = new MotivosRetail();
        motivosRetail.setAmbiente(rs.getString("Ambiente"));
        motivosRetail.setProducto(rs.getString("Producto"));
        motivosRetail.setNombreTipologia(rs.getString("nombreTipologia"));
        motivosRetail.setIdTipologiaElement(rs.getString("idTipologiaElement"));
        motivosRetail.setNombreMotivo(rs.getString("nombreMotivo"));
        motivosRetail.setIdMotivo(rs.getString("idMotivo"));
        motivosRetail.setEstadoMotivo(rs.getString("estadoMotivo"));
        motivosRetail.setMotivoTransaccional(rs.getString("motivoTransaccional"));
        return motivosRetail;
    }
}
