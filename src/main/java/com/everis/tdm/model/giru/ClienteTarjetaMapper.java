package com.everis.tdm.model.giru;

import lombok.SneakyThrows;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

import static com.everis.tdm.security.Decryption.decrypt;

public class ClienteTarjetaMapper implements RowMapper<ClienteTarjeta> {
    @SneakyThrows
    public ClienteTarjeta mapRow(ResultSet rs, int arg1) throws SQLException {
        ClienteTarjeta data = new ClienteTarjeta();
        data.setTipodocumento(rs.getString("tipodocumento"));
        data.setNumerodocumento(rs.getString("numerodocumento"));
        data.setNumerotarjeta(rs.getString("numerotarjeta"));
        data.setTipotarjeta(rs.getString("tipotarjeta"));
        data.setMarcatarjeta(rs.getString("marcatarjeta"));
        data.setEstadoproducto(rs.getString("estadoproducto"));

        data.setTipoproducto(rs.getString("tipoproducto"));
        data.setMonedacuenta(rs.getString("monedacuenta"));
        data.setNumerocuenta(rs.getString("numerocuenta"));
        data.setTipodecuenta(rs.getString("tipodecuenta"));

        return data;
    }
}
