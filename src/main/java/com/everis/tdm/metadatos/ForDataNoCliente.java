package com.everis.tdm.metadatos;

public class ForDataNoCliente {

    public static String generarNombresAleatorios() {
        String[] nombres = {"Andrea", "David", "Baldomero", "Balduino", "Baldwin", "Baltasar", "Barry", "Bartolo",
                "Bartolomé", "Baruc", "Baruj", "Candelaria", "Cándida", "Canela", "Caridad", "Carina", "Carisa",
                "Caritina", "Carlota", "Baltazar"};
        return nombres[(int) (Math.floor(Math.random() * ((nombres.length - 1) + 1) + 0))];// + " ";
    }

    public static String generarApellidosAleatorios() {
        String[] apellidos = {"Gomez", "Guerrero", "Cardenas", "Cardiel", "Cardona", "Cardoso", "Cariaga", "Carillo",
                "Carion", "Castiyo", "Castorena", "Castro", "Grande", "Grangenal", "Grano", "Grasia", "Griego",
                "Grigalva"};
        return apellidos[(int) (Math.floor(Math.random() * ((apellidos.length - 1) + 1) + 0))];
    }

    public static String generarOperador() {
        String[] operador = {"CLARO", "ENTEL", "MOVISTAR", "BITEL"};
        return operador[(int) (Math.floor(Math.random() * ((operador.length - 1) + 1) + 0))];
    }

    public static String generarEmail() {
        String[] email = {"RUSI@GMAIL.COM", "ASLA@GMAIL.COM", "LUCI@GMAIL.COM", "ONDA@GMAIL.COM"};
        return email[(int) (Math.floor(Math.random() * ((email.length - 1) + 1) + 0))];
    }

    public static String generarCelular() {
        String[] Celular = {"916724342", "916724343"};
        return Celular[(int) (Math.floor(Math.random() * ((Celular.length - 1) + 1) + 0))];
    }

    public static String generarFechaNacimiento() {
        String[] FechaNacimiento = {"20111985", "20111982"};
        return FechaNacimiento[(int) (Math.floor(Math.random() * ((FechaNacimiento.length - 1) + 1) + 0))];
    }

    public static String generarSexo() {
        String[] sexo = {"MASCULINO", "FEMENINO"};
        return sexo[(int) (Math.floor(Math.random() * ((sexo.length - 1) + 1) + 0))];
    }

    public static String generarOcupacion() {
        String[] ocupacion = {"ADMINISTRADOR", "ECONOMISTA", "ARQUITECTO", "ABOGADO"};
        return ocupacion[(int) (Math.floor(Math.random() * ((ocupacion.length - 1) + 1) + 0))];
    }

    public static String generarRuc() {
        String[] ruc = {"20521586134", "20389230724", "20100122368"};
        return ruc[(int) (Math.floor(Math.random() * ((ruc.length - 1) + 1) + 0))];
    }

    public static String generarDepartamento() {
        String[] departamento = {"LIMA"};
        return departamento[(int) (Math.floor(Math.random() * ((departamento.length - 1) + 1) + 0))];
    }

    public static String generarProvincia() {
        String[] provincia = {"LIMA"};
        return provincia[(int) (Math.floor(Math.random() * ((provincia.length - 1) + 1) + 0))];
    }

    public static String generarDistrito() {
        String[] distrito = {"LIMA"};
        return distrito[(int) (Math.floor(Math.random() * ((distrito.length - 1) + 1) + 0))];
    }

    public static String generarTipoDeVia() {
        String[] tipoDeVia = {"CALLE"};
        return tipoDeVia[(int) (Math.floor(Math.random() * ((tipoDeVia.length - 1) + 1) + 0))];
    }

    public static String generarNombreVia() {
        String[] nombreVia = {"DANIEL"};
        return nombreVia[(int) (Math.floor(Math.random() * ((nombreVia.length - 1) + 1) + 0))];
    }

}
