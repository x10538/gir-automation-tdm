package com.everis.tdm.dao.giru;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.everis.tdm.model.giru.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.util.List;

@Component
public class giruDAO implements giruImpl {

    public static JdbcTemplate jdbc;

    @Autowired
    public giruDAO(DataSource dataSource) {
        jdbc = new JdbcTemplate(dataSource);
    }

    public void sp_insertCliente(String tipodocumento, String nrodocumento, String operador, String telefono, String cantidadtarjetas) {
        String SQL = "{call insertarCliente(?,?,?,?,?)}";
        jdbc.update(SQL, tipodocumento, nrodocumento, operador, telefono, cantidadtarjetas);
    }

    public void sp_insertUsuario(String Usuario, String Area, String Contrasena, String Rol) {
        String SQL = "{call insertarUsuario(?,?,?,?)}";
        jdbc.update(SQL, Usuario, Contrasena, Rol, Area);
    }

    public void sp_updatePasswordUsuario(String Usuario, String Contrasena) {
        String SQL = "{call updatePasswordUsuario(?,?)}";
        jdbc.update(SQL, Usuario, Contrasena);
    }

    public void sp_deleteUsuario(String Usuario) {
        String SQL = "{call deleteUsuario(?)}";
        jdbc.update(SQL, Usuario);
    }

    public void sp_insertClienteTarjeta(String tipodocumento, String numerodocumento, String numerotarjeta, String tipotarjeta, String marcatarjeta, String estadotarjeta) {
        String SQL = "{call insertarClienteTarjeta(?,?,?,?,?,?)}";
        jdbc.update(SQL, tipodocumento, numerodocumento, numerotarjeta, tipotarjeta, marcatarjeta, estadotarjeta);
    }

    public void sp_insertTarjeta(String numtarjeta, String cvv, String fecha, String estado, String tipo) {
        String SQL = "{call insertarTarjeta(?,?,?,?,?)}";
        jdbc.update(SQL, numtarjeta, cvv, fecha, estado, tipo);
    }

    public void sp_updateMillas(String codigoUnico, int millasTarjeta) {
        String SQL = "{call updateMillas(?,?)}";
        jdbc.update(SQL, codigoUnico, millasTarjeta);
    }

    @Override
    public List<Cliente> sp_getClientes() {
        String SQL = "{call getClientes}";
        return jdbc.query(SQL,
                new ClienteMapper());
    }


    public List<Cliente> spGetClientByDocumentType() {
        String SQL = "{call spGetClientByDocumentType(?)}";
        return jdbc.query(SQL, new ClienteMapper());
    }

    public List<Cliente> sp_getClientePorTarjetas(String vcantidadtarejtas) {
        String SQL = "{call getClientePorCantidadTarjetas(?)}";
        return jdbc.query(SQL, new ClienteMapper(), new Object[]{vcantidadtarejtas});
    }

    public List<Usuario> sp_getUsuarioPorRol(String vrol) {
        String SQL = "{call getUsuarioPorRol(?)}";
        return jdbc.query(SQL, new UsuarioMapper(), new Object[]{vrol});
    }

    public List<User> sp_getUsersPorRolYAmbiente(String vrol, String vambiente) {
        String SQL = "{call getUsersPorRolYAmbiente(?,?)}";
        return jdbc.query(SQL, new UserMapper(), new Object[]{vrol, vambiente});
    }

    public List<Usuario> sp_getUsuarioPorRolYArea(String vrol, String varea) {
        String SQL = "{call getUsuarioPorRolYArea(?,?)}";
        return jdbc.query(SQL, new UsuarioMapper(), new Object[]{vrol, varea});
    }

    public List<ClienteTarjeta> sp_getClienteTarjetaPorFiltros(String tipotarjeta, String marcatarjeta, String estadotarjeta) {
        String SQL = "{call getClienteTarjetaPorFiltros(?,?,?)}";
        return jdbc.query(SQL, new ClienteTarjetaMapper(), new Object[]{tipotarjeta, marcatarjeta, estadotarjeta});
    }

    //TODO: TIPOLOGIA
    public List<ObtenerTipologiaId> sp_getTipologiaId(String ambiente, String producto, String tipoTramite, String nombreTipologia) {
        String SQL = "{call usp_ObtenerTipologia(?,?,?,?)}";
        return jdbc.query(SQL, new ObtenerTipologiaIdMapper(), new Object[]{ambiente, producto, tipoTramite, nombreTipologia});
    }
    // Tipologia RETAIL - POC
    public List<ObtenerTipologiaIdRetail> sp_getTipologiaIdRetail(String ambiente, String producto, String tipoTramite, String nombreTipologia,String EstadoTipologia) {
        String SQL = "{call usp_ObtenerTipologiaRetail(?,?,?,?,?)}";
        return jdbc.query(SQL, new ObtenerTipologiaIdMapperRetail(), new Object[]{ambiente, producto, tipoTramite, nombreTipologia,EstadoTipologia});
    }

    //TODO: MOTIVO - NUEVO
    public List<ObtenerMotivoId> sp_getMotivoId(String ambiente, String producto, String nombreTipologia) {
        String SQL = "{call usp_ObtenerMotivo(?,?,?)}";
        return jdbc.query(SQL, new ObtenerMotivoIdMapper(), new Object[]{ambiente, producto, nombreTipologia});
    }

    //Motivo - RETAIL - POC
    public List<ObtenerMotivoIdRetail> sp_getMotivoIdRetail(String ambiente, String producto, String nombreTipologia, String estadoMotivo,String motivoTransaccional) {
        String SQL = "{call usp_ObtenerMotivoRetail(?,?,?,?,?)}";
        return jdbc.query(SQL, new ObtenerMotivoIdMapperRetail(), new Object[]{ambiente, producto, nombreTipologia,estadoMotivo,motivoTransaccional});
    }
    //TODO: TIPOLOGIA COMERCIAL
    public List<ObtenerTipologiaIdComercial> sp_getTipologiaIdComercial(String ambiente, String producto, String tipoTramite, String nombreTipologia) {
        String SQL = "{call usp_ObtenerTipologiaComercial(?,?,?,?)}";
        return jdbc.query(SQL, new ObtenerTipologiaIdComercialMapper(), new Object[]{ambiente, producto, tipoTramite, nombreTipologia});
    }

    //TODO: MOTIVO COMERCIAL - NUEVO
    public List<ObtenerMotivoIdComercial> sp_getMotivoIdComercial(String ambiente, String producto, String nombreTipologia) {
        String SQL = "{call usp_ObtenerMotivoComercial(?,?,?)}";
        return jdbc.query(SQL, new ObtenerMotivoIdComercialMapper(), new Object[]{ambiente, producto, nombreTipologia});
    }

    //TODO: RESOLUCION RETAIL - NUEVO
    public List<EscenarioValidacion> sp_getEscenarioValidacionPorFiltros(String tipoproducto, String tipotramite, String tipologia, String areaValidadoraOpcional) {
        String SQL = "{call getEscenarioValidacionPorFiltros(?,?,?,?)}";
        return jdbc.query(SQL, new EscenarioValidacionMapper(), new Object[]{tipoproducto, tipotramite, tipologia, areaValidadoraOpcional});
    }

    //TODO: RESOLUCION COMERCIAL - NUEVO
    public List<resolucionComercial> sp_getResolucionComercialPorFiltros(String tipoproducto, String tipotramite, String tipologia, String areaValidadoraOpcional) {
        String SQL = "{call getResolucionComercialPorFiltros(?,?,?,?)}";
        return jdbc.query(SQL, new resolucionComercialMapper(), new Object[]{tipoproducto, tipotramite, tipologia, areaValidadoraOpcional});
    }

    public List<ClienteTarjeta> sp_getClienteDocumentoProductoPorFiltros(String tipoproducto, String tipodocumento) {
        String SQL = "{call getClienteDocumentoProductoPorFiltros(?,?)}";
        return jdbc.query(SQL, new ClienteTarjetaMapper(), new Object[]{tipoproducto, tipodocumento});
    }

    public List<ClienteTarjeta> sp_getClienteDocumentoProductoEstadoPorFiltros(String tipoproducto, String tipodocumento, String tipoestado) {
        String SQL = "{call getClienteDocumentoProductoEstadoPorFiltros(?,?,?)}";
        return jdbc.query(SQL, new ClienteTarjetaMapper(), new Object[]{tipoproducto, tipodocumento,tipoestado});
    }

    public List<ClienteTarjeta> sp_getClienteTarjetaDebitoPorFiltros(String tipotarjeta, String estadoproducto) {
        String SQL = "{call getClienteTarjetaDebitoPorFiltros(?,?)}";
        return jdbc.query(SQL, new ClienteTarjetaMapper(), new Object[]{tipotarjeta, estadoproducto});
    }
    public void sp_insertrRegistroReclamoPOC(String NroReclamo, String Tipologia, String FechaRegistro, String UsuarioRegistro, String Perfil, String TipoDocumento, String NumeroDocumento,  String Tramite, String Atencion) {
        String SQL = "{call insertarRegistroReclamoPOC(?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, NroReclamo, Tipologia, FechaRegistro, UsuarioRegistro, Perfil, TipoDocumento, NumeroDocumento,Tramite, Atencion);
    }

    public void sp_insertrRegistroReclamo(String NroReclamo, String Tipologia, String EscenarioRegistrado, String EstadoTarjeta, String MarcaTarjeta, String TipoTarjeta, String UsuarioRegistro, String Perfil, String TipoDocumento, String NumeroDocumento, String NumeroTarjeta, String NumeroCuenta, String Tramite, String Atencion, String AgregarComentarios, String RespuestaCliente, String AdjuntarArchivo, String MedioRespuesta, String NumeroCelular, String NuevoRegistroRespuesta, String RazonOperativa, String TipoCMPMalConcretado, String Operador, String FechaConsumoMalConcretado, String TipoRazonOperativa, String MotivoSucedidoTarjeta, String SeleccionarNoBloqueado, String DescripcionNoBloqueado, String NombreComercio, String CantidadTarjetas) {
        String SQL = "{call insertarRegistroReclamo2(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, NroReclamo, Tipologia, EscenarioRegistrado, EstadoTarjeta, MarcaTarjeta, TipoTarjeta, UsuarioRegistro, Perfil, TipoDocumento, NumeroDocumento, NumeroTarjeta, NumeroCuenta, Tramite, Atencion, AgregarComentarios, RespuestaCliente, AdjuntarArchivo, MedioRespuesta, NumeroCelular, NuevoRegistroRespuesta, RazonOperativa, TipoCMPMalConcretado, Operador, FechaConsumoMalConcretado, TipoRazonOperativa, MotivoSucedidoTarjeta, SeleccionarNoBloqueado, DescripcionNoBloqueado, NombreComercio, CantidadTarjetas);
    }

    public void sp_insertrRegistroGestion(String NroTramite, String Usuario, String FuenteData, String TipoRegistro, String TipoDocumento, String NumDocumento, String IdGestion, String Llamada, String Comentario) {
        String SQL = "{call insertarRegistroGestion(?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, NroTramite, Usuario, FuenteData, TipoRegistro, TipoDocumento, NumDocumento, IdGestion, Llamada, Comentario);
    }

    public List<RegistroGestion> sp_getGestionPorTipoRegistroYDocumento(String tiporegistro, String documento, String numdocumento) {
        String SQL = "{call getGestionPorTipoRegistroYDocumento(?,?,?)}";
        return jdbc.query(SQL, new RegistroGestionMapper(), new Object[]{tiporegistro, documento, numdocumento});
    }
    public void sp_updateRegistroBenni(String codigoUnico) {
        String SQL = "{call updateRegistroBenni(?)}";
        jdbc.update(SQL, codigoUnico);
    }

    //--------------------------------------------------------------- Renzo
    // llamando directamente al procedimiento almacenado
    //@Override
    public void sp_insertReclamo(String tipologia, String medioRespuesta) {
        String SQL = "{call insertarReclamo(?,?)}";
        jdbc.update(SQL, tipologia, medioRespuesta);
    }

    public void spIngresarDatos(String documentType
            , String tipoProducto
            , String documentNumber
            , String codigoUnico
            , String marcaTarjeta
            , String codMarca
            , String marcaTipoTarjeta
            , String codTipoTarjeta
            , String nroTarjeta
            , String estadoTarjeta
            , String panTarjeta
            , String canal
            , String tipoCodigoUnico) {
        String SQL = "{call [spIngresarDatos](?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, documentType
                , tipoProducto
                , documentNumber
                , codigoUnico
                , marcaTarjeta
                , codMarca
                , marcaTipoTarjeta
                , codTipoTarjeta
                , nroTarjeta
                , estadoTarjeta
                , panTarjeta
                , canal
                , tipoCodigoUnico);
    }

    public void spIngresarDatosTarjeta(String uniqueCode, String accountNumber, String currency, String creditLine, List<CreditCard> creditCard ) {
        String SQL1 = "{call [spSetAccountCreditCard](?,?,?,?)}";
        jdbc.update(SQL1, uniqueCode, accountNumber, currency, creditLine);

        for (CreditCard tc : creditCard) {
            String SQL2 = "{call [spSetCreditCard](?,?,?,?,?,?,?,?,?)}";
            jdbc.update(SQL2, accountNumber, tc.getCardNumber(), tc.getBeneficiary(), tc.getMark(), tc.getSituation(), tc.getStatus(), tc.getTypeCode(), tc.getType(), tc.getActivationDate());
        }
    }

    public void spDeleteCreditCardDataByCardNumber(String cardNumber) {
        String SQL = "{call [spDeleteCreditCardByCardNumber](?)}";
        jdbc.update(SQL, cardNumber);
    }
    public void spDeleteDebitCardDataByCardNumber(String cardNumber) {
        String SQL = "{call [spDeleteDebitCardByCardNumber](?)}";
        jdbc.update(SQL, cardNumber);
    }

    public void spSetAccount(String uniqueCode, String type, String productCode, String subType, String subProductCode,
                             String currency, String balance, String availableBalance, String status,
                             String office, String accountNumber, String joint_type, String cci, String id ) {
        String SQL = "{call [spSetAccount](?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, uniqueCode, type, productCode, subType, subProductCode, currency, balance, availableBalance, status, office, accountNumber, joint_type, cci, id);
    }

    public void spDeleteAccountByAccountNumber(String uniqueCode) {
        String SQL = "{call [spDeleteAccountByAccountNumber](?)}";
        jdbc.update(SQL, uniqueCode);
    }

    public void spIngresarDatosCliente(ClientData client) {
        String SQL = "{call [spSetClientData](?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, client.getUniqueCode(), client.getTypeClient(), client.getDocumentClient(), client.getTypeDocument(),
                client.getFirstFirstName(), client.getSecondFirstName(),client.getFirstLastName(), client.getSecondLastName(),
                client.getCellNumber(), client.getProfileLevel(), client.getProfileLevelStatus(), client.getProfileLevelChange(),
                client.getSegment(),client.getSituation(),client.getServiceLevel(),client.getPersonType(),client.getCustomerPEP());
    }
//    public void spIngresarDatosNoCliente(NoClientData client) {
//        String SQL = "{call [spSetNoClientData](?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
//        jdbc.update(SQL, client.getTypeClient()
//                , client.getDocumentClient(),client.getTypeDocument(),
//                client.getFirstFirstName(), client.getSecondFirstName(),client.getFirstLastName(), client.getSecondLastName(),
//                client.getDateBirth(), client.getOperator(), client.getPhoneNumber(),client.getEmailPersonal(), client.getCompanyName(),
//                client.getCreationDate(),client.getEmailCompany());
//    }
    public void spIngresarDatosClienteAddress(List<ClientAddress> clientAddressList,String uniqueCode) {
        ObjectMapper mapper = new ObjectMapper();
        List<ClientAddress> clientAddressMap=mapper.convertValue(clientAddressList, new TypeReference<List<ClientAddress>>() {});

        for (ClientAddress clientAddress:clientAddressMap) {
            String SQL = "{call [spSetClientAddress](?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
            jdbc.update(SQL, uniqueCode,clientAddress.getUseCode(),clientAddress.getSequenceId(),
                    clientAddress.getDepartment(),clientAddress.getProvince(),clientAddress.getDistrict(),
                    clientAddress.getStreetType(),clientAddress.getStreetName(),clientAddress.getStreetNumber(),
                    clientAddress.getBlock(),clientAddress.getLot(),clientAddress.getApartment(),clientAddress.getNeighborhood(),
                    clientAddress.getLandmark(),clientAddress.getPostalCode(),clientAddress.getCountry(),clientAddress.getUbigeo(),
                    clientAddress.getType(),clientAddress.getUseType(),clientAddress.getDate(),clientAddress.getStatus(),
                    clientAddress.getFlagStandard());

        }
    }
    public void spIngresarDatosClienteCompany(ClientCompany clientCompany) {
        String SQL = "{call [spSetClientCompany](?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, clientCompany.getUniqueCode(), clientCompany.getName(), clientCompany.getAdmissionDate(),
                clientCompany.getAddress(), clientCompany.getPhoneNumber(), clientCompany.getExtension(),
                clientCompany.getPositionDescripcion(), clientCompany.getOccupation(), clientCompany.getIndependent(),
                clientCompany.getUbigeo(), clientCompany.getEmployeeIndicator());
    }
    public void spIngresarDatosClienteEmail(List<ClientEmail> clientEmailList,String uniqueCode) {
        ObjectMapper mapper = new ObjectMapper();
        List<ClientEmail> clientEmailMap=mapper.convertValue(clientEmailList, new TypeReference<List<ClientEmail>>() {});

        for (ClientEmail clientEmail:clientEmailMap) {
            String SQL = "{call [spSetClientEmail](?,?,?,?)}";
            jdbc.update(SQL, uniqueCode, clientEmail.getId(), clientEmail.getType(), clientEmail.getValue());
        }
    }
    public void spIngresarDatosClientePhone(List<ClientPhone> clientPhoneList,String uniqueCode) {
        ObjectMapper mapper = new ObjectMapper();
        List<ClientPhone> clientPhoneMap=mapper.convertValue(clientPhoneList, new TypeReference<List<ClientPhone>>() {});

        for (ClientPhone clientPhone:clientPhoneMap) {
            String SQL = "{call [spSetClientPhone](?,?,?,?,?,?)}";
            jdbc.update(SQL, uniqueCode, clientPhone.getId(), clientPhone.getType(),
                    clientPhone.getDepartment(), clientPhone.getNumber(), clientPhone.getExtension());
        }
    }
    public void spUpdateClientData(ClientData clientData) {
        String SQL = "{call [spUpdateClientData](?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL, clientData.getUniqueCode(), clientData.getTypeClient(), clientData.getDocumentClient(),
                clientData.getTypeDocument(), clientData.getFirstFirstName(), clientData.getSecondFirstName(),
                clientData.getFirstLastName(), clientData.getSecondLastName(), clientData.getCellNumber(),
                clientData.getProfileLevel(), clientData.getProfileLevelChange(),clientData.getSegment(),
                clientData.getSituation(),clientData.getServiceLevel(),clientData.getPersonType(),clientData.getCustomerPEP());
    }

    public void spSetValidationDataNew(String codServicio, String description, String componente, String api_ifx, String version) {
        String SQL = "{call [spSetValidationDataNew](?,?,?,?,?)}";
        jdbc.update(SQL, codServicio, description, componente, api_ifx, version);
    }

    public List<ClientData> spSearchNumberClients(String documentClient) {
        String SQL = "{call [spSearchNumberClients](?)}";
        return jdbc.query(SQL, new ClientDataMap(), documentClient);
    }

    public List<ClientData> spGetClientByDocumentType(String typeDocument) {
        String SQL = "{call [spGetClientByDocumentType](?)}";
        return jdbc.query(SQL, new ClientDataMap(), typeDocument);
    }

    public List<TipoGestion> spGetTipoGestionByGestionPadre(String descripcionTipoGestion) {
        String SQL = "{call [spGetTipoGestionByGestionPadre](?)}";
        return jdbc.query(SQL, new TipoGestionMap(), descripcionTipoGestion);
    }

    public List<EscenarioResolucionRetail> spGetEscenarioResolucionRetail(String tipoProducto, String tipoTramite, String tipologia, String estadoTipologia, String ambiente) {
        String SQL = "{call [spGetEscenarioResolucionRetail](?,?,?,?,?)}";
        return jdbc.query(SQL, new EscenarioResolucionRetailMap(), tipoProducto, tipoTramite, tipologia, estadoTipologia, ambiente);
    }

    public List<ClientDataCreditCard> spGetClienteAndTC_By_typeDocument_beneficiary_status(String typeDocument, String beneficiary, String status) {
        String SQL = "{call [spGetClienteAndTC_By_typeDocument_beneficiary_status](?,?,?)}";
        return jdbc.query(SQL, new ClientDataCreditCardMap(), typeDocument, beneficiary, status);
    }
    public List<ClientDataDebitCard> spGetClienteAndTD_By_typeDocument_beneficiary_status(String typeDocument, String status_code) {
        String SQL = "{call [spGetClienteAndTD_By_typeDocument_beneficiary_status](?,?)}";
        return jdbc.query(SQL, new ClientDataDebitCardMap(), typeDocument, status_code);
    }

    public List<MotivosRetail> spGetMotivoRetail_By_ambiente_producto_nombreTipologia(String ambiente, String producto, String nombreTipologia) {
        String SQL = "{call [spGetMotivoRetail_By_ambiente_producto_nombreTipologia](?,?,?)}";
        return jdbc.query(SQL, new MotivosRetailMap(), ambiente, producto, nombreTipologia);
    }


    public List<ClientData> spGetClientWithAccountDebitByTypeAccountAndCurrencyAndTypeDocument(String typeAccount, String currency, String typeDocument) {
        String SQL = "{call [spGetClientWithAccountDebitByTypeAccountAndCurrencyAndTypeDocument](?,?,?)}";
        return jdbc.query(SQL, new ClientDataMap(), typeAccount, currency, typeDocument);
    }

    public List<ClientData> spGetClientePorTipoDoc(String typeDocument) {
        String SQL = "{call spGetClientePorTipoDoc(?)}";
        return jdbc.query(SQL, new ClientDataMap(), new Object[]{typeDocument});
    }



    public List<ClientDataTypeDocument> spGetClientNaturalPersonByTypeDocument(String typeDocument) {
        String SQL = "{call [spGetClientNaturalPersonByTypeDocument](?)}";
        return jdbc.query(SQL, new ClientDataTypeDocumentMap(), typeDocument);
    }

    public List<ClientData> spGetClientWithoutCreditCards() {
        String SQL = "{call [spGetClientWithoutCreditCards]}";
        return jdbc.query(SQL, new ClientDataMap());
    }

   /* public List<CreditCard_> spSearchCreditCard(String cardNumber) {
        String SQL = "{call [spSearchCreditCard](?)}";
        return jdbc.query(SQL, new CreditCardMap_(), cardNumber);
    }*/

    public List<Account> spSearchAccount(String accountdNumber) {
        String SQL = "{call [spSearchAccount](?)}";
        return jdbc.query(SQL, new AccountMap(), accountdNumber);
    }

    public List<ClientData2> spSearchClient(String uniqueCode) {
        String SQL = "{call [spSearchClient](?)}";
        return jdbc.query(SQL, new ClientDataMap2(), uniqueCode);
    }
//    public List<NoClientData> spSearchNoClient(String document) {
//        String SQL = "{call [spSearchNoClient](?)}";
//        return jdbc.query(SQL, new NoClientDataMap(), document);
//    }
    public void spEmptyDataBase() {
        String SQL = "{call [spEmptyDataBase]()}";
        jdbc.update(SQL);
        System.out.println("Se ha eliminado los datos de la Base ");
    }
    public List<ClientCreditCard> spGetCreditCardWithMovement(String movimiento) {
        String SQL = "{call [spGetCreditCardWithMovement](?)}";
        return jdbc.query(SQL, new ClientCreditCardMap(),movimiento);
    }

    public List<ClientCreditCard> spGetCreditCardForCheckMovement() {
        String SQL = "{call [spGetCreditCardForCheckMovement]()}";
        return jdbc.query(SQL, new ClientCreditCardMap());
    }

    public void spSetDebitCardData(DebitCard debitCard) {
        String SQL = "{call [spSetDebitCardData](?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)}";
        jdbc.update(SQL,debitCard.getUniqueCode(),debitCard.getCardNumber(),debitCard.getStatus_code(),
                debitCard.getStatus_description(),debitCard.getBlock_id(),debitCard.getCustomer_id(),debitCard.getCustomer_type(),
                debitCard.getBranchId(),debitCard.getOpeningDate(),debitCard.getDueDate(),debitCard.getCardGroup(),
                debitCard.getCardName(),debitCard.getRequestId(),debitCard.getStatus(),debitCard.getEmbossingtype(),
                debitCard.getCardSubType(),debitCard.getCardTechType(),debitCard.getCardType(),debitCard.getLastReferenceNumber(),
                debitCard.getPreviousReferenceNumber(),debitCard.getLock_reasonId());
    }

    //**********************CREACION PUNTO DE SERVICIO**********************
    //PERSONA NATURAL Y EMPRESA
    public List<Client> spGetClientByTypeDocumentAndTypeAccountAndCurrency(String typeDocument, String typeAccount, String currency) {
        String SQL = "{call [spGetClientByTypeDocumentAndTypeAccountAndCurrency](?,?,?)}";
        return jdbc.query(SQL, new ClientMap(), typeDocument, typeAccount, currency);
    }

}
