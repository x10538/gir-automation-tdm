package com.everis.tdm.utils;

public class UtilChange {
    public static String changeDocumentType(String dniTdm) {
        String tipoDoc = "";
        if (dniTdm.equals("DNI")){
            tipoDoc="01";
        } else if (dniTdm.equals("CE")){
            tipoDoc="03";
        }else if (dniTdm.equals("PASAPORTE")){
            tipoDoc="05";
        }else if (dniTdm.equals("RUC")){
            tipoDoc="02";
        }else if (dniTdm.equals("PTM")){
            tipoDoc="09";
        }
        return tipoDoc;
    }
    public static String changeMail(String type) {
        String tipoDoc = "";
        if (type.equals("DNI")){
            tipoDoc="fausto.interpruebas@gmail.com";
        } else if (type.equals("RUC")) {
            tipoDoc="interbankstestfausto.2023@gmail.com";
        }
        return tipoDoc;

    }
}
