package com.everis.tdm.utils;

import com.everis.tdm.configurations.AuthenticationConfiguration;
import com.everis.tdm.configurations.ServicesConfiguration;
import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.path.json.JsonPath;
import net.serenitybdd.rest.SerenityRest;
import org.apache.http.HttpStatus;

import static io.restassured.RestAssured.given;

public class Security {
    public static String channelToken;
    private static JsonPath response;

    public static String createAccessToken() {
        String body = "{\"username\":\""+ AuthenticationConfiguration.getTokenUser()+"\",\"password\":\"" +
                AuthenticationConfiguration.getTokenPass()+"\"}";
        try {
            return "Bearer " + given()
                    .header("Authorization", "Basic c3BhOnBhc3N3b3Jk")
                    .contentType(ContentType.JSON)
                    .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getSecuritySubscriptionKey())
                    .body(body)
                    .post(ServicesConfiguration.getBaseUrl() + "/api/uat/login")
                    .then()
                    .assertThat()
                    .statusCode(HttpStatus.SC_OK)
                    .extract()
                    .response()
                    .jsonPath()
                    .getString("access_token");
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
    private static String getBasicChannel(String channel) {
        switch (channel) {
            case "APP": return "Basic YXBwOjBMUjRqTEVhVVhhaHVlTXk";
            case "BPI": return "Basic YnBpOm9ia1ZOcDRhZnJBSGJ5Vnc";
            case "WBP": return "Basic d2JwOjRGNmNyT2o1Z05sZG4wMXM";
        }
        return null;
    }

    public static String createAccessTokenDigital(String channel) {
        try {
            return channelToken="Bearer " + SerenityRest.given()
                    .header("Authorization", getBasicChannel(channel))
                    .contentType(ContentType.URLENC)
                    .header("Ocp-Apim-Subscription-Key", "2bf94a64237c45f7a88b865777de9b69")
                    .formParam("scope", "all")
                    .formParam("grant_type", "client_credentials")
                    .post("https://apis.uat.interbank.pe/api/clientes/oauth/token")
                    .then()
                    .extract().response().jsonPath().getString("access_token");

        } catch (Exception e) {
                throw new RuntimeException(e);
        }
    }

    public static void deleteToken(String token) {
        given().header("Authorization", "Basic c3BhOnBhc3N3b3Jk")
                .contentType(ContentType.URLENC)
                .header("Ocp-Apim-Subscription-Key", ServicesConfiguration.getSecuritySubscriptionKey())
                .header("OAuth-Token", token)
                .post(ServicesConfiguration.getBaseUrl() + "/api/uat/logout");
    }

    public static String generoTokenBus(String scope) {
        RestAssured.useRelaxedHTTPSValidation();
            response = given()
                    .header("Content-Type", "application/x-www-form-urlencoded")
                    .header("Authorization", "Bearer " + ApiCommons.AUTHORIZATION_TOKEN_VALUE)
                    .header("PCIauth", "Basic " + ApiCommons.PCI_AUTH)
                    .formParam("grant_type", "password")
                    .formParam("username", ApiCommons.CREDENTIAL_USERNAME)
                    .formParam("password", ApiCommons.CREDENTIAL_PASSWORD)
                    .formParam("client_id", ApiCommons.OPC_APIM_SUSCRIPTION_KEY)
                    .formParam("client_secret", ApiCommons.OPC_APIM_SUSCRIPTION_SECRECT)
                    .formParam("scope", scope)
                    .post(ApiCommons.URL_BASE + "/oauthproviderh1/oauth2/token")
                    .then()
                    .extract().jsonPath();
        String token = response.get("access_token");
//        System.out.println("TOKEN " + token);
        return token;
    }
}
