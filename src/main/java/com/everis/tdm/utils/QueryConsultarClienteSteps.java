package com.everis.tdm.utils;

import net.thucydides.core.annotations.Step;

public class QueryConsultarClienteSteps {
    QueryConsultaCliente queryConsultaCliente;

    @Step("Consultar Cliente por documento")
    public void consultarCliente(String tipoDoc, String numDoc){
        queryConsultaCliente = new QueryConsultaCliente();
        queryConsultaCliente.seArmaElCuerpoDePeticionConsultarCliente(tipoDoc,numDoc);
        queryConsultaCliente.agregoLosEncabezadosEnLaPeticion();
        queryConsultaCliente.validoLaRespuestaObtenida();
    }

}
