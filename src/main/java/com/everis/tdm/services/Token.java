package com.everis.tdm.services;

import com.everis.tdm.model.giru.Cliente;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.CoreMatchers;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static com.everis.tdm.commons.Util.getRegexValue;
import static com.everis.tdm.commons.Util.getTemplate;
import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;


public class Token {
    private static final Logger LOGGER = LoggerFactory.getLogger(Token.class);

    static private final String CREAR_CLIENTE_URL = "https://apidev.digital.interbank.pe/security/v1/oauth/token";

    // static private final String TEMPLATE_CREAR_CLIENTE = "/request/crearCliente.xml";

    public static String crearToken() {
        LOGGER.info("Customer Creation started...");
        String token = "";
        RestAssured.useRelaxedHTTPSValidation();
        //    cli.setTipoDocumento("DNI");
        //    cli.setNumeroDocumento(String.valueOf(Util.generateDocumentRandom()));
        // TODO: Controlar cantidad de intentos, evitar saturar el servicio
        while (true) {
            //      String bodyRequest = getTemplate(TEMPLATE_CREAR_CLIENTE).replace("{numDocumento}", "cli.getNumeroDocumento()");
            Response res = given().
                    header("Content-Type", "application/x-www-form-urlencoded").
                    header("Ocp-Apim-Subscription-Key", "332a09a749cd4b0eb2d760af339d569f").
                    header("Authorization", "Basic WWk0TGVMclgzc1I3c0JuYjp3UWkyeGZaTEpqREwzZmI5").
                    formParam("grant_type", "client_credentials").

                    formParam("scope", "token:application").
                    //            body(bodyRequest).
                            when().
                    post(CREAR_CLIENTE_URL);

            if (res.statusCode() == 200) {
                token = res.getBody().jsonPath().getString("access_token");
                System.out.println("se obtuvo token: " + token);


                break;
            } else {
                LOGGER.info("Request error:  DEBUG MODE");
            }
            LOGGER.info("CREACION DESACTIVADA");
        }
        return token;
    }

}
