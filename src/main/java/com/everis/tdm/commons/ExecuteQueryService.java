package com.everis.tdm.commons;

import com.everis.tdm.Do;
import com.everis.tdm.model.giru.*;
import com.everis.tdm.utils.QueryService;

import static com.everis.tdm.Do.searchNumberClients;

public class ExecuteQueryService {
    private static String dniCliente = "";
    private static String rucCliente = "";
    private static int newDni = 0;
    private static int newRuc = 0;
    private static ClientCreditCard clientCreditCard = new ClientCreditCard();

    public static String generarDNINoClienteAleatorio(String tipo) {
        newDni = (int) (Math.random() * 90000000 + 9999999);
        dniCliente = String.valueOf(newDni);
        //String dniCliente = "16164230";
        System.out.println(dniCliente);
        //if (QueryService.verifyNewDni(dniCliente, tipo).equals("CLIENTE") || searchNumberClients(dniCliente).size() > 0) {
        if (QueryService.verifyNewDni(dniCliente, tipo).equals("CLIENTE")) {
            generarDNINoClienteAleatorio(tipo);
        }
        return dniCliente;
    }

    public static String generarRUCNoClienteAleatorio(String tipo) {

        newRuc = (int) (Math.random() * 90000000+9999999);
        rucCliente = String.valueOf(newRuc);
        rucCliente=Integer.valueOf("20"+rucCliente.toString()).toString();
        int[] numsarr={5,4,3,2,7,6,5,4,3,2};
        int valida = 0;
        for(int i=0; i<10; i++){
            int nums = Character.getNumericValue(rucCliente.charAt(i));
            valida += numsarr[i]*nums;
        }
        int b=valida/11;
        int c=11-(valida-b*11);
        if(c==10)
            c=0;
        if(c==11)
           c=1;
        rucCliente=rucCliente+c;
        System.out.println("Ruc Generado: "+rucCliente);
        if (QueryService.verifyNewDni(rucCliente, tipo).equals("CLIENTE")) {
            generarDNINoClienteAleatorio(tipo);
        }
        return rucCliente;
    }
    public static String generarClienteSiguiente(int documentNumber, String tipo) {
        dniCliente = String.valueOf(documentNumber);
        System.out.println(dniCliente);
        if (!QueryService.verifyNewDni(dniCliente, tipo).equals("CLIENTE") || searchNumberClients(dniCliente).size() > 0) {
            documentNumber = documentNumber + 1;
            generarClienteSiguiente(documentNumber, tipo);
        }
        return dniCliente;
    }

   /* public static ClientData ConsultarDataCliente(String tipo) {
        String documento = generarNoClienteAleatorio(tipo);
        return QueryService.getClientData(documento, tipo);
    }*/

    public static String ConsultarDatoNoCliente(String tipo) {
        String documento="";
        if(tipo.equals("DNI")){
            documento = generarDNINoClienteAleatorio(tipo);
        }else{
            documento = generarRUCNoClienteAleatorio(tipo);
        }

        return documento;
    }


    public static void consultarDatosTarjeta(String dni) {
        //CreditCard_ creditCard = findCreditCard(dni);
        QueryService.findCreditCard(dni);
        //return creditCard;
    }

    public static Account consultarDatosTarjeta2(String dni) {
        Account account = QueryService.getAccountAndCreditCard(dni);
        return account;
    }

    public static void consultarCuenta(String codigoUnico, String type) {
        //getAccountAndCreditCard(codigoUnico);
        QueryService.getCustomerAccount(codigoUnico, type);
    }

    public static void consultarTarjetasCreditoCliente(String codigoUnico) {
        QueryService.getAccountAndCreditCard(codigoUnico);
        QueryService.findCreditCard(codigoUnico);
    }

    public static ClientCreditCard executeCheckStatusMovements(String typo, int contador) {
        clientCreditCard = Do.spGetCreditCardForCheckMovement().get(0);
        System.out.println("Data consultada: "+clientCreditCard.getCardNumber());
        contador++;
        if (!QueryService.checkMovement(clientCreditCard.getCardNumber()).equals(typo) && contador<40){
            System.out.println("Consultando Movimientos");
            executeCheckStatusMovements(typo, contador);
        }
        return clientCreditCard;
    }public static ClientCreditCard executeGetMovements(String type) {
        clientCreditCard = Do.spGetCreditCardWithMovement(type).get(0);
        System.out.println("Data consultada: "+clientCreditCard.getCardNumber());
        return clientCreditCard;
    }

//    public static ClientData executeCheckProfileLevel(ClientData clientData, int contador) {
//        clientCreditCard = Do.spGetCreditCardForCheckMovement().get(0);
//        System.out.println("Data consultada: "+clientCreditCard.getCardNumber());
//        contador++;
//        if (!checkMovement(clientCreditCard.getCardNumber()).equals(typo) && contador<40){
//            System.out.println("Consultando Movimientos");
//            executeCheckStatusMovements(typo, contador);
//        }
//        return clientCreditCard;
//    }
}
