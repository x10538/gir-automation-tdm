package com.everis.tdm.commons;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.everis.tdm.Do;
import com.everis.tdm.model.giru.*;
import com.everis.tdm.utils.QueryService;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.ResourceUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Iterator;
import java.util.List;

import static com.everis.tdm.Do.*;

public class UploadData {

    private static final Logger LOGGER = LoggerFactory.getLogger(UploadData.class);
    public static void leerExcelDocumentos(String data, String function) throws FileNotFoundException {
        File exce = ResourceUtils.getFile("classpath:" + archivo(data, function));
        try (FileInputStream file = new FileInputStream(exce)) {
            Sheet sheet = WorkbookFactory.create(file).getSheetAt(0);
            Iterator<Row> rowIterator = sheet.iterator();
            Row row;
            int contador = 0;
            while (rowIterator.hasNext()) {
                contador++;
                if(contador == 1) rowIterator.next();
                row = rowIterator.next();
                if (data.equals("CLIENTES")) {
                    LOGGER.info(row.getCell(0).getStringCellValue() + " " + row.getCell(1).getStringCellValue());
                    try{
                        uploadFullDataClient(row.getCell(0).getStringCellValue(), row.getCell(1).getStringCellValue());
                    } catch (Exception e) {
                        LOGGER.error("ERROR - AL PROCESAR UPLOAD FULL DATA CLIENT:" + e.getMessage());
                    }
                }
            }
            LOGGER.info("Número de registros leídos: " + contador);
        } catch (Exception e) {
            LOGGER.error("ERROR EXCEL:" + e.getMessage());
        }
    }

    public static void uploadClientData(ClientData clientData) {
        //consulto api información de cliente
        if (clientData.getTypeClient().equals("CLIENTE")) {
            //Ingreso información del response en la tabla ClientData
            spIngresarDatosCliente(clientData);
            //Ingreso información del response en la tabla ClientAddress
            spIngresarDatosClienteAddress(clientData.getAddress(), clientData.getUniqueCode());
            //Ingreso información del response en la tabla ClientEmail
            spIngresarDatosClienteCompany(clientData.getCompany());
            //Ingreso información del response en la tabla ClientCompany
            spIngresarDatosClienteEmail(clientData.getEmail(), clientData.getUniqueCode());
            //Ingreso información del response en la tabla ClientPhone
            spIngresarDatosClientePhone(clientData.getPhone(), clientData.getUniqueCode());
        }
    }

    public static void uploadAccountData(ClientData clientData,String typeDocument) {
        //consulto api información de cliente
        if (clientData.getTypeClient().equals("CLIENTE")) {
            //consulta api información de cuentas del cliente
            List<Account> account = QueryService.getCustomerAccount(clientData.getDocumentClient(), typeDocument);
            if (account.size() > 0) {
                //Si existe información de la CUENTA en la tabla Account, esta se elimina
                Do.spDeleteAccountByAccountNumber(clientData.getUniqueCode());

                ObjectMapper mapper = new ObjectMapper();
                List<Account> AccountMap=mapper.convertValue(account, new TypeReference<List<Account>>() {});


                for (Account a: account) {
                    //Registro las CUENTAS del cliente en la tabla Account
                    spIngresarDatosCuenta(a, clientData.getUniqueCode());
                }
                System.out.println("Cuentas agregadas: " + account.size());
            }
        }
    }
    public static String archivo(String data, String function) {
        String rutaArchivo = null;
        if (data.equalsIgnoreCase("Clientes")) {
            LOGGER.info("[LECTURA ARCHIVO]"+data+"|"+function);
            rutaArchivo = "Data/"+function+".xlsx";
        }
        return rutaArchivo;
    }
    public static void uploadCreditCardClient (ClientData clientData, String type) {
        if (clientData.getTypeClient().equals("CLIENTE")) {
            List<AccountCreditCard> accountCreditCard = QueryService.getAccountFullData(clientData);
            if (accountCreditCard.size() > 0) {
                for (AccountCreditCard ac : accountCreditCard) {
                    //Si existe información de la TARJETA en la tabla AccountCreditCard, esta se elimina
                    for (CreditCard tc : ac.getCreditCard()) {
                        Do.spDeleteCreditCardDataByCardNumber(tc.getCardNumber());
                    }
                    //Registro las TARJETAS del cliente en la tabla Account
                    spIngresarDatosTarjeta(ac, clientData.getUniqueCode());
                }
                System.out.println("TC agregadas: " + accountCreditCard.size());
            }
        }
    }
    public static void uploadDebitCardClient (ClientData clientData, String type) {
        if (clientData.getTypeClient().equals("CLIENTE")) {
            List<DebitCard> debitCards = QueryService.getDebitCard(clientData.getUniqueCode());
            if (!clientData.getTypeClient().equals("SIN_REGISTROS") && debitCards.size() > 0) {
                for (DebitCard dc : debitCards) {
                    spDeleteDebitCardDataByCardNumber(dc.getCardNumber());
                    spIngresarDatosTarjetaDebito(dc);
                }
                System.out.println("TD agregadas: " + debitCards.size());
            }
        }
    }
}
