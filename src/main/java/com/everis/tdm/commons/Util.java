package com.everis.tdm.commons;

import org.apache.commons.io.IOUtils;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Util {

    public static String getTemplate(String templatePath) {
        try {
            return IOUtils.toString(new ClassPathResource(templatePath).getInputStream(), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static int generateDocumentRandom() {
        Random rnd = new Random();
        int n = 1000 + rnd.nextInt(9000);
        return 10210000 + n;
    }

    public static String getRegexValue(String res, String regex) {
        try {
            String retorno = "";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(res);
            matcher.find();
            if (matcher.groupCount() > 0) {
                retorno = matcher.group(1);
            }
            return retorno;
        } catch (Exception var5) {
            new Exception("No results found to the regular expression");
            return "No results found to the regular expression";
        }
    }
}
